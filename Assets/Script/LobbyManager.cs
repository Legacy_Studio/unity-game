﻿using UnityEngine;
using UnityEngine.SceneManagement;

using Photon.Pun;
using Photon.Realtime;

using TMPro;

public class LobbyManager : MonoBehaviourPunCallbacks
{
    public GameObject PlayersListFrame;
    public GameObject PlayerNamePrefab;

    private void Awake()
    {
        PhotonNetwork.AutomaticallySyncScene = true;
    }
    // Start is called before the first frame update
    void Start()
    {
        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        // base.OnConnectedToMaster();
        print("Connected to master!");

        PhotonNetwork.JoinLobby(TypedLobby.Default);
    }

    public override void OnCreatedRoom()
    {
        // base.OnCreatedRoom();
        Debug.Log(PhotonNetwork.CurrentRoom.Name +" Room has been created");
        
    }

    public override void OnJoinedLobby()
    {
        // base.OnJoinedLobby();
        // Debug.Log("We joined to the lobby");
    }

    public override void OnJoinedRoom()
    {
        // base.OnJoinedRoom();
        Debug.Log("We joined to the room!");
        foreach (var _player in PhotonNetwork.PlayerList) {
            // Debug.Log("Number: " +_player.ActorNumber + "\n PLayer Name:" +_player.NickName);
            GameObject _name = Instantiate(PlayerNamePrefab) as GameObject;
            _name.GetComponentInChildren<TextMeshProUGUI>().text = _player.NickName;
            _name.transform.SetParent(PlayersListFrame.transform, false);
            RectTransform _rect = _name.GetComponent<RectTransform>();
            _rect.anchorMin = new Vector2(0, 1);
            _rect.anchorMax = new Vector2(1, 1);
            _rect.pivot = new Vector2(0.5f, 0.5f);
            _rect.anchoredPosition = new Vector2(0f, ((_player.ActorNumber * -100) + 50));
            // Debug.Log("Position: " + new Vector3(_rect.position.x, ((_player.ActorNumber * -100) + 50), _rect.position.z));
            // _rect.position = new Vector3(_rect.position.x, ((_player.ActorNumber * -100) + 50), _rect.position.z);
        }
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        base.OnJoinRoomFailed(returnCode, message);
        Debug.Log("We couldn't join to the room!");
    }

    public void EnterArena() {
        PhotonNetwork.LoadLevel(2);
    }
    
}
