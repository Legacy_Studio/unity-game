﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Pun;
using Photon.Realtime;

public class Getsuga : MonoBehaviour
{
    public int Cost;
    public int Force;
    public int Damage;
    public int Speed;
    public float MaxLifetime;
    public LayerMask whatCanDestroy;

    int Owner;
    float Lifetime;
    // Start is called before the first frame update
    private void Start()
    {
        Lifetime = MaxLifetime;
    }
    // Update is called once per frame
    void Update()
    {
        transform.position += transform.forward * Time.deltaTime * Speed;
        if (Lifetime > 0)
        {
            Lifetime -= Time.deltaTime;
        }
        else {
            Destroy(gameObject);
        }
    }

    public void SetOwner(int _ownerNr) {
        Owner = _ownerNr;
    }

    #region RPC Method
    [PunRPC]
    void SetSpecialsOwnerRPC(int _ownerNr) {
        Debug.Log("Tried to set it!" + _ownerNr);
        SetOwner(_ownerNr);
    }
    #endregion

    private void OnTriggerEnter(Collider other)
    {
        // removed other.gameObject != Owner && from if
        if (other.CompareTag("Player") &&  other.gameObject.GetComponent<PhotonView>().OwnerActorNr != Owner && other.GetComponent<p_status>().isPlayerAlive() == true) {
            Vector3 dir = new Vector3(transform.position.x, other.gameObject.transform.position.y,  transform.position.z) - new Vector3(other.gameObject.transform.position.x, other.gameObject.transform.position.y, other.gameObject.transform.position.z);
            PhotonView photonView = PhotonView.Get(other.gameObject);

            if (other.gameObject.GetComponent<p_motor>().isPerformingBlock() == false)
            {
                photonView.RPC("TakeDamagePhoton",
                            RpcTarget.Others,
                            new object[] {
                                (float)Damage,
                                dir.normalized,
                                (float)(-1 * Force)
                            });
                /*
                other.gameObject.GetComponent<p_status>().TakeDamage(Damage);
                other.gameObject.GetComponent<Rigidbody>().AddForce(
                    dir.normalized * -1 * Force,
                    ForceMode.Impulse
                );
                */
            }
            else{
                photonView.RPC("TakeDamagePhoton",
                            RpcTarget.Others,
                            new object[] {
                                (float)(Damage / 2),
                                dir.normalized,
                                (float)(-Force / 2)
                            });
                photonView.RPC("UseEnergyRPC", RpcTarget.Others, new object[] { (float)(Damage / 2) });
                /*
                other.gameObject.GetComponent<p_status>().TakeDamage(Damage / 2);
                other.gameObject.GetComponent<p_status>().UserEnergy(Damage / 2);
                other.gameObject.GetComponent<Rigidbody>().AddForce(
                    dir.normalized * -1 * ( Force / 2) ,
                    ForceMode.Impulse
                );
                */
            }
            Destroy(gameObject);
            // PhotonNetwork.Destroy(gameObject);
        }

        if (other.CompareTag("Special")) {
            Destroy(gameObject);
            Destroy(other.gameObject);
            /*
            PhotonNetwork.Destroy(gameObject);
            PhotonNetwork.Destroy(other.gameObject);
            */
        }
    }
}
