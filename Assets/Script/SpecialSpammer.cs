﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialSpammer : MonoBehaviour
{

    public GameObject Model;
    public float SpawnInterval;
    public Transform SpawnLocation;

    bool isSpawning = false;
    // Start is called before the first frame update
    void Start()
    {
    }

    public void StartSpawning() {
        if (isSpawning == false)
        {
            isSpawning = true;
            StartCoroutine(Spawn());
        }
    }

    IEnumerator Spawn() {
        SpawnSpecial();
        yield return new WaitForSeconds(SpawnInterval);
        if (isSpawning)
            StartCoroutine(Spawn());
    }

    public void StopSpawning() {
        isSpawning = false;
    }

    public void SpawnSpecial() {
        Instantiate(Model, SpawnLocation.position, SpawnLocation.rotation);
    }

}
