﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraTPS : MonoBehaviour
{
    public float mouseSensitivity = 10f;
    public float distance = 2f;
    public Transform lookTarget;
    public Vector3 pitchMinMax = new Vector2(-10f, 85f);
    public Vector3 Offset = Vector3.zero;

    private float Yaw;
    private float Pitch;

    Vector3 targetRotation = Vector3.zero;
    float damping;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        FollowTarget();
	}

    void FollowTarget() {
        Yaw += Input.GetAxis("Mouse X") * mouseSensitivity;
        Pitch -= Input.GetAxis("Mouse Y") * mouseSensitivity;
        Pitch = Mathf.Clamp(Pitch, pitchMinMax.x, pitchMinMax.y);

        targetRotation = new Vector3(Pitch, Yaw);

        transform.eulerAngles = targetRotation;

        transform.position = lookTarget.position - transform.forward * distance + Offset;
        //transform.LookAt(lookTarget.up);
    }

    #region Public Methods
    public void SetTarget(Transform _target) {
        lookTarget = _target;
    }
    #endregion
}
