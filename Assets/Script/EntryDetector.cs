﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntryDetector : MonoBehaviour
{
    public string Command;
    public Transform Spawn;
    public GameObject UI;
    public GameManager GM;
    public SpecialSpammer spammer;

    private void OnTriggerExit(Collider other)
    {
         if (other.CompareTag("Player") && UI.activeInHierarchy == false)
             DisableUI();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && GM.TutorialState() == false) {
            other.gameObject.GetComponent<p_status>().SetSpawner(Spawn);
            UI.SetActive(true);
            if (Command == "Start" && spammer != null)
            {
                spammer.StartSpawning();
            }
            else if (Command == "Finish" && GM != null) {
                GM.FinishTutorial();
                spammer.StopSpawning();
            }
            else
            {
                if (spammer != null)
                    spammer.StopSpawning();
            }
        }
    }
    
    public void DisableUI() {
        UI.SetActive(false);
    }
}
