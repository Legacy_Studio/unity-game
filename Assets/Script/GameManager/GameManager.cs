﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using UnityEngine.SceneManagement;
using TMPro;

using Photon.Pun;
using Photon.Realtime;

public class GameManager : MonoBehaviourPunCallbacks
{
    public bool isPhotonEnabled;
    public GameObject TestCharacter;

    public GameObject Cam;
    public GameObject UICanvas;
    public Image ShunpoBorder;
    public TextMeshProUGUI ShunpoCounter;
    public TextMeshProUGUI RoundCountUI;
    public TextMeshProUGUI RoundTimerUI;

    public Transform Spawn1;
    public Transform Spawn2;

    public bool CursorVisibility;
    public int MaxRound;
    public float RoundTime;
    public float FreezeTime;
    public float RoundBreakTime;

    public GameObject PlayerPrefab;
    [SerializeField]
    public GameObject[] Players;

    private int CurrentRound = 0;
    private bool isTutorialOver = false;
    private bool isPlayersRepositioned = false;
    private bool isGameOver = false;
    private bool isRoundStarted = false;
    private bool isBreakTime = false;
    private float RoundTimer;
    private float FreezeTimer;

    private void Awake()
    {
        Cursor.visible = CursorVisibility;
    }

    private void Start()
    {
        if (PlayerPrefab == null)
        {
            Debug.LogError("<Color=Red><a>Missing</a></Color> playerPrefab Reference. Please set it up in GameObject 'Game Manager'", this);
        }
        else {
            if (p_status.LocalPlayerInstance == null)
            {
                Debug.Log("We spawned a Character!");
                SpawnPlayer();
            }
        }
        // SpawnPlayer();
        StartRound();
    }

    private void Update()
    {
        if(isGameOver == false)
            RoundProgression();
    }

    public override void OnLeftRoom()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public bool TutorialState() {
        return isTutorialOver;
    }

    public void FinishTutorial() {
        isTutorialOver = true;
    }

    #region Public Methods

    public void LeaveRoom() {
        PhotonNetwork.LeaveRoom();
    }
    
    #endregion

    #region Round Methods
    public bool RoundState() {
        return isRoundStarted;
    }

    public void RoundProgression()
    {
        if (isRoundStarted == false)
        {
            FreezeTimerControl();
        }
        else
        {
            RoundTimerControl();
            /*
            if (AlivePlayers() > 1)
            {
                RoundTimerControl();
            }
            else {
                EndRound();
            }
            */
        }
    }

    public int AlivePlayers(){
        int HowMany = 0;
        foreach (var _player in Players) {
            if (_player.GetComponent<p_status>().isPlayerAlive() == true)
                HowMany++;
        }
        return HowMany;
    }

    public void FreezeTimerControl() {
        if (FreezeTimer > 0)
        {
            FreezeTimer -= Time.deltaTime;
            int Minutes = (int)(FreezeTimer / 60);
            int Seconds = (int)(FreezeTimer % 60);
            RoundTimerUI.text = Minutes.ToString("00.") + ":" + Seconds.ToString("00.");
            RoundTimerUI.color = new Color32(234, 52, 52, 255);
        }
        else
        {
            isRoundStarted = true;
            isBreakTime = false;
            RoundTimer = RoundTime;
        }
    }

    public void RoundTimerControl() {
        if (RoundTimer > 0)
        {
            RoundTimer -= Time.deltaTime;
            int Minutes = (int)(RoundTimer / 60);
            int Seconds = (int)(RoundTimer % 60);
            RoundTimerUI.text = Minutes.ToString("00.") + ":" + Seconds.ToString("00.");
            RoundTimerUI.color = new Color32(255, 255, 255, 255);
        }
        else
        {
            EndRound();
        }
    }

    public void StartRound()
    {
        // Position Players to their Spawn
        CurrentRound = CurrentRound + 1;
        RoundCountUI.text = CurrentRound.ToString();
        // Start Freeze timer
        FreezeTimer = FreezeTime;
        // Start Round Timer
        RoundTimer = RoundTime;
        isRoundStarted = false;
        //RepositionPlayer();
    }

    public void EndRound() {
        if (CurrentRound >= MaxRound)
        {
            // End game
            // Debug.Log("All Rounds Has been played");
            isGameOver = true;
        }
        else {
            // Calculate points
            // Debug.Log("Round has been ended");
            if (isBreakTime == false)
                StartCoroutine(NextRound());
        }
    }
    #endregion

    #region Spawn related methods
    void SpawnPlayer()
    {
        GameObject _player;
        if (isPhotonEnabled)
        {
            Transform _spawn = PhotonNetwork.LocalPlayer.NickName == "Nomio" ? Spawn1 : Spawn2;
            _player = PhotonNetwork.Instantiate(this.PlayerPrefab.name, _spawn.position, _spawn.rotation);
        } else
        {
            _player = TestCharacter;
        }
        
        /*
        if (PhotonNetwork.LocalPlayer.NickName == "Nomio")
        {
            Players[0] = _player;
        }
        else {
            Players[1] = _player;
        }
        */
        // Enable Player dependent components if the player is local
        Cam.SetActive(true);
        Cam.GetComponent<CameraTPS>().SetTarget(_player.transform);
        _player.GetComponent<p_motor>().SetCamera(Cam.GetComponent<Camera>());

        UICanvas.SetActive(true);
        _player.GetComponent<p_status>().SetUI();

        _player.GetComponent<p_motor>().SetShunpoBorder(ShunpoBorder);
        _player.GetComponent<p_motor>().SetShunpoCounterUI(ShunpoCounter);
    }

    void RepositionPlayer() {
        if (isPlayersRepositioned == false && isPhotonEnabled) {
            /*
            if (p_status.LocalPlayerInstance.GetComponent<p_status>().isPlayerAlive() == false)
            {
                p_status.LocalPlayerInstance.GetComponent<p_status>().Respawn();
            }
            */
            p_status.LocalPlayerInstance.GetComponent<p_status>().Respawn();
            p_status.LocalPlayerInstance.GetComponent<PhotonView>().RPC("RespawnRPC", RpcTarget.Others);
            // Reposition the players to their spawn
            Transform _spawn = PhotonNetwork.LocalPlayer.NickName == "Nomio" ? Spawn1 : Spawn2;
            p_status.LocalPlayerInstance.transform.position = _spawn.position;
            isPlayersRepositioned = true;
        }
        
    }
    #endregion

    IEnumerator NextRound() {
        isBreakTime = true;
        yield return new WaitForSeconds(RoundBreakTime);
        isPlayersRepositioned = false;
        RepositionPlayer();
        // Start Next Round
        StartRound();
    }
}
