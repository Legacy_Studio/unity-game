﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using Photon.Pun;

public class p_status : MonoBehaviourPunCallbacks, IPunObservable
{
    public bool isInTutorial;
    public int Team;
    public int MaxHealth;
    public int MaxEnergy;
    public int energyRegenRate;
    public float combatExitInterval;
    public float energyRegenInterval;
    public Slider HealthBar;
    public Slider EnergyBar;

    [Tooltip("The local player instance. Use this to know if the local player is represented in the Scene")]
    public static GameObject LocalPlayerInstance;

    Transform Spawner;
    p_motor motor;
    float CurrentHealth;
    float CurrentEnergy;
    float lastInCombat;
    float lastEnergyRegen;
    bool isAlive = true;

    private void Awake() {
        if (photonView.IsMine)
        {
            p_status.LocalPlayerInstance = this.gameObject;
        }
        // #Critical
        // we flag as don't destroy on load so that instance survives level synchronization, thus giving a seamless experience when levels load.

        // Enable it when using Photon
        DontDestroyOnLoad(this.gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        motor = GetComponent<p_motor>();
        CurrentHealth = MaxHealth;
        if (HealthBar != null) {
            HealthBar.maxValue = MaxHealth;
            HealthBar.value = CurrentHealth;
        }

        CurrentEnergy = MaxEnergy;

        if (EnergyBar != null)
        {
            EnergyBar.maxValue = MaxEnergy;
            EnergyBar.value = CurrentEnergy;
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            // We own this player: send the others our data
            // stream.SendNext(IsFiring);
            stream.SendNext(isAlive);
            stream.SendNext(CurrentHealth);
            stream.SendNext(CurrentEnergy);
        }
        else
        {
            // Network player, receive data
            // this.IsFiring = (bool)stream.ReceiveNext();
            this.isAlive = (bool)stream.ReceiveNext();
            this.CurrentHealth = (float)stream.ReceiveNext();
            this.CurrentEnergy = (float)stream.ReceiveNext();
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (Time.time - lastInCombat < combatExitInterval)
        {
            // Debug.Log("In Combat!");
        } else{
            motor.UpdateInCombat(false);
        }

        if (motor.IsInCombat() == false) {
            RegenEnergy();
        }

        if (transform.position.y <= -5 && isInTutorial == true) {
            transform.position = Spawner.position;
        }
    }

    #region Public Methods
    public void Dead() {
        if (CurrentHealth > 0)
        {
            CurrentHealth = 0;
            HealthBar.value = CurrentHealth;
        }
        isAlive = false;
    }

    public void Respawn() {
        Debug.Log("Respawned!");
        isAlive = true;
        CurrentHealth = MaxHealth;
        HealthBar.value = CurrentHealth;
    }

    public bool isPlayerAlive() {
        return isAlive;
    }

    public void TakeDamage(float amount) {
        CurrentHealth -= amount;
        if(HealthBar)
            HealthBar.value = CurrentHealth;

        motor.TakeDamage();

        if (CurrentHealth <= 0) {
            if (isInTutorial == true)
            {
                CurrentHealth = MaxHealth;
                HealthBar.value = CurrentHealth;
            }
            else {
                Dead();
                motor.Die();
            }
        }
    }

    public void UserEnergy(float Amount) {
        if (isInTutorial == false) {
            CurrentEnergy -= Amount;
            EnergyBar.value = CurrentEnergy;
        }
    }

    public float GetCurrentEnergy() {
        return CurrentEnergy;
    }
    public float GetCurrentHealth() {
        return CurrentHealth;
    }
    public void SetLastCombatTime(float time) {
        lastInCombat = time;
    }

    public void SetSpawner(Transform _spawner) {
        Spawner = _spawner;
    }

    public void SetUI() {
        HealthBar = GameObject.Find("HealthBar").GetComponent<Slider>();
        EnergyBar = GameObject.Find("EnergyBar").GetComponent<Slider>();
    }
    #endregion

    #region Private methods
    void RegenEnergy() {
        if (CurrentEnergy < MaxEnergy && Time.time - lastEnergyRegen >= energyRegenInterval)
        {
            CurrentEnergy += energyRegenRate;
            EnergyBar.value = CurrentEnergy;
            lastEnergyRegen = Time.time;
        }

        if (CurrentEnergy > MaxEnergy)
            CurrentEnergy = MaxEnergy;
    }
    #endregion

    #region RPC Methods
    [PunRPC]
    void TakeDamagePhoton(float amount, Vector3 dir, float force) {
        Debug.Log("Take Damage via RPC!!!");
        TakeDamage(amount);
        GetComponent<Rigidbody>().AddForce(
                dir.normalized * force,
                ForceMode.Impulse
            );
    }

    [PunRPC]
    void UseEnergyRPC(float amount) {
        UserEnergy(amount);
    }

    [PunRPC]
    void RespawnRPC() {
        Respawn();
    }
    #endregion

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Key")) {
            SceneManager.LoadScene("MainMenu");
        }
    }
}
