﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Photon.Pun;

public class p_hit_detection : MonoBehaviour
{
    public Color RayTracer;
    public p_motor motor;
    public GameObject[] nodes;

    private bool isAttacked = false;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(HitCor());
    }

    IEnumerator HitCor()
    {
        while (true)
        {
            if (motor.isPerformingAttack())
            {
                List<Vector3> nodes1 = new List<Vector3>();
                for (int c = 0; c < nodes.Length; ++c)
                {
                    nodes1.Add(nodes[c].transform.position);
                }
                yield return new WaitForEndOfFrame();
                // now draw raycasts from prev pos to current pos
                for (int c = 0; c < nodes.Length; ++c)
                {
                    Ray ray = new Ray(nodes1[c], nodes[c].transform.position - nodes1[c]);
                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit, (nodes[c].transform.position - nodes1[c]).magnitude))
                    { // HIT!!
                        if (hit.transform.tag == "Player" && isAttacked == false && !hit.transform.GetComponent<PhotonView>().IsMine && hit.transform.GetComponent<p_status>().isPlayerAlive())
                        {
                            Vector3 dir = new Vector3(transform.position.x, hit.transform.position.y, transform.position.z) - new Vector3(hit.transform.transform.position.x, hit.transform.transform.position.y, hit.transform.transform.position.z);
                            PhotonView photonView = PhotonView.Get(hit.transform.gameObject);
                            // Debug.Log("Is " + hit.transform.GetComponent<PhotonView>().Owner + "Blocking:  " + hit.transform.GetComponent<p_motor>().isPerformingBlock());
                            if (hit.transform.GetComponent<p_motor>().isPerformingBlock() == false)
                            {

                                // Debug.Log("Apply Damage to: " + hit.transform.GetComponent<PhotonView>().Owner);
                                Debug.Log("Before attack: " + hit.transform.gameObject.GetComponent<p_status>().GetCurrentHealth());
                                photonView.RPC(
                                    "TakeDamagePhoton",
                                    RpcTarget.All,
                                    new object[] {
                                        (float)15f,
                                        dir.normalized,
                                        (float)-15f
                                    });
                                /*
                                hit.transform.gameObject.GetComponent<p_status>().TakeDamage(15);
                                hit.transform.gameObject.GetComponent<Rigidbody>().AddForce(
                                    dir.normalized * -1 * 15,
                                    ForceMode.Impulse
                                );
                                */
                            }
                            else {
                                Debug.Log("He was blocking!");
                                photonView.RPC(
                                    "TakeDamagePhoton",
                                    RpcTarget.All,
                                    new object[] {
                                        (float)5f,
                                        dir.normalized,
                                        (float)-(15f /2)
                                    });
                                /*
                                hit.transform.gameObject.GetComponent<p_status>().TakeDamage(5);
                                hit.transform.gameObject.GetComponent<Rigidbody>().AddForce(
                                    dir.normalized * -1 * (15 / 2),
                                    ForceMode.Impulse
                                );
                                */
                            }
                            Debug.Log("Before After: " + hit.transform.gameObject.GetComponent<p_status>().GetCurrentHealth());
                            isAttacked = true;
                        }

                        if (hit.transform.tag == "RedBox") {
                            Destroy(hit.transform.gameObject);
                        }
                    }
                    Debug.DrawLine(nodes1[c], nodes[c].transform.position, RayTracer * Time.deltaTime, 3f, false);
                }
            }
            else
            {
                isAttacked = false;
                yield return new WaitForEndOfFrame();
            }
        }
    }
}
