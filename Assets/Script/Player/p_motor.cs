﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

using Photon.Pun;

public class p_motor : MonoBehaviourPunCallbacks, IPunObservable
{
    public LayerMask whatIsGround;
    public Animator anim;
    public GameObject model;
    public GameObject SpecialModel;
    public GameObject SpecialModelPhoton;
    public Transform SpawnPoint;
    public Transform FootLocation;
    public Image ShunpoBorder;
    public int MaxShunpoCharge;
    public float movespeed;
    public float jumpForce;
    public float floatForceMin;
    public float floatForceMax;
    public float footRadius;
    public float desiredRotationSpeed;
    public float ShunpoRange;
    public float ShunpoSpeed;
    public float ShunpoInterval;
    public float blockInterval;
    public float attackInterval;
    public float SpecialInterval;
    public float KnockbackInterval;

    // Animation References
    public AnimationClip SpecialAnimation;

    // Private Variables 
    TextMeshProUGUI ShunpoCounter;
    Camera cam;
    Rigidbody rb;
    Vector3 desiredMoveDirection = Vector3.zero;
    Vector3 velocity = Vector3.zero;
    [SerializeField]
    float fallMultiplier = 2.5f;
    float shunpoMultiplier = 1f;
    float nextAttack;
    float nextShunpo;
    float shunpoCharger;
    int shunpoCharge;
    int comboCounter = 0;
    bool modelStatus = true;
    bool isPlayerGrounded = false;
    bool isInCombat = false;
    bool canMove = true;
    bool isBlocking = false;
    bool isAttacking = false;
    bool isSpecialCasting = false;

    #region Network Related Variables
    Vector3 networkPosition;
    Quaternion networkRotation;
    #endregion
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();

        shunpoCharge = MaxShunpoCharge;
        ShunpoCounter.text = shunpoCharge.ToString();
        #region Should be enabled in Case of Using Photon Network
        /*
        if (GetComponent<PhotonView>().IsMine == true){
            shunpoCharge = MaxShunpoCharge;
            ShunpoCounter.text = shunpoCharge.ToString();
        }
        */
        #endregion
    }

    // Update is called once per frame
    void Update()
    {
        // In case of Photon is Enabled
        if (GetComponent<PhotonView>().IsMine == true) {
            
        }
        #region Should be inside of Photon isMine
        if (shunpoCharge < MaxShunpoCharge)
        {
            if (shunpoCharger > 0)
            {
                shunpoCharger -= Time.deltaTime;
                // this if is only debug purpose. Needs to be removed
                // if(GetComponent<p_controller>().isMine == true)
                ShunpoBorder.fillAmount = (ShunpoInterval - shunpoCharger) / ShunpoInterval;
            }
            else
            {
                ++shunpoCharge;
                // this if is only debug purpose. Needs to be removed
                // if (GetComponent<p_controller>().isMine == true)
                ShunpoCounter.text = shunpoCharge.ToString();
                if (shunpoCharge == MaxShunpoCharge)
                {
                    shunpoCharger = 0;
                    // this if is only debug purpose. Needs to be removed
                    // if (GetComponent<p_controller>().isMine == true)
                    ShunpoBorder.fillAmount = 1;
                }
                else
                {
                    shunpoCharger = ShunpoInterval;
                    // this if is only debug purpose. Needs to be removed
                    // if (GetComponent<p_controller>().isMine == true)
                    ShunpoBorder.fillAmount = 0;
                }
            }
        }
        #endregion
        UpdateShunpoStatus();
        CheckFootGrounded();
    }

    void FixedUpdate(){
        if (rb.velocity.y < 0)
        {
            rb.velocity += Vector3.up * Physics.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        }
        PerformMoveAndRotation();
        // FixedUpdatePlayersPosition();
    }

    #region Private Methods
    void CheckFootGrounded() {
        Collider[] colls = Physics.OverlapSphere(FootLocation.position, footRadius, whatIsGround);

        if (colls.Length > 0)
            isPlayerGrounded = true;
        else
            isPlayerGrounded = false;

        anim.SetBool("isGrounded", isPlayerGrounded);
    }
    #endregion
    #region Public Methods
    public void SetCamera(Camera _cam) {
        cam = _cam;
    }

    public void SetShunpoBorder(Image _ShunpoBorder) {
        ShunpoBorder = _ShunpoBorder;
    }

    public void SetShunpoCounterUI(TextMeshProUGUI _ShunpoCounter) {
        ShunpoCounter = _ShunpoCounter;
    }

    public void Move(Vector3 _velocity){
        velocity = _velocity;
    }

    public void Shunpo(){
        if (shunpoCharge > 0)
        {
            Debug.Log("Shunpo!");
            if (velocity != Vector3.zero)
                StartCoroutine(PerformShunpo(desiredMoveDirection));
                // rb.MovePosition(rb.position + (desiredMoveDirection * ShunpoRange));
            else
                StartCoroutine(PerformShunpo(rb.transform.forward));
                //rb.position = rb.position + rb.transform.forward * ShunpoRange;
        }
    }

    public void PerformMoveAndRotation() {
        if (velocity != Vector3.zero && GameObject.Find("GameManager").GetComponent<GameManager>().RoundState() == false) {
            velocity = Vector3.zero;
        }
        if (velocity != Vector3.zero && canMove == true)
        {
            Vector3 forward = cam.transform.forward;
            Vector3 right = cam.transform.right;

            forward.y = 0f;
            right.y = 0f;

            forward.Normalize();
            right.Normalize();

            anim.SetFloat("speed", 1);
            if (velocity != Vector3.zero && isAttacking == false ){
                desiredMoveDirection = (forward * velocity.z + right * velocity.x);
                if (desiredMoveDirection.magnitude > 1)
                    desiredMoveDirection.Normalize();
            }
            if(isAttacking == false){
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desiredMoveDirection), desiredRotationSpeed);
                // Mid air movement restriction
                float distance = desiredMoveDirection.magnitude * movespeed * Time.deltaTime;
                RaycastHit hit;
                // Debug.Log("Distance: " + distance);
                // Debug.Log("Value: " + rb.SweepTest(desiredMoveDirection, out hit, distance));
                if (rb.SweepTest(desiredMoveDirection, out hit, distance) && isPlayerGrounded == false)
                {
                    rb.velocity = new Vector3(0f, rb.velocity.y, 0f);
                } else
                {
                    rb.MovePosition(rb.position + (desiredMoveDirection * movespeed * shunpoMultiplier * Time.deltaTime));
                }
            }
        } else {
            anim.SetFloat("speed", 0);
            desiredMoveDirection = Vector3.zero;
        }
    }

    public void Attack()
    {
        transform.rotation = Quaternion.LookRotation(cam.transform.forward);
        StartCoroutine(PerformAttack());
        transform.eulerAngles = new Vector3(0f,cam.transform.eulerAngles.y,0f);
    }

    public void Special() {
        if (GetComponent<p_status>().GetCurrentEnergy() > SpecialModel.gameObject.GetComponent<Getsuga>().Cost)
        {
            transform.rotation = Quaternion.LookRotation(cam.transform.forward);
            StartCoroutine(PerformSpecial());
            GetComponent<PhotonView>().RPC(
                "SpecialRPC", 
                RpcTarget.Others, 
                new object[] {
                    SpawnPoint.position,
                    SpawnPoint.rotation,
                    GetComponent<PhotonView>().OwnerActorNr
                });
            transform.eulerAngles = new Vector3(0f, cam.transform.eulerAngles.y, 0f);
        }
        else {
            Debug.Log("Not enough Energy!");
        }
    }

    public void Jump() {
        if (isPlayerGrounded == true) {
            rb.AddForce(transform.up * jumpForce, ForceMode.VelocityChange);
            anim.SetTrigger("jump");
        }
    }

    public void FloatInAir() {
        if (isPlayerGrounded == false)
        {
            isInCombat = true;
            GetComponent<p_status>().SetLastCombatTime(Time.time);
            // float dynamicFloatForce = rb.velocity.y < 0 ? -(rb.velocity.y * 2) : (rb.velocity.y * 2);
            // rb.AddForce(transform.up * floatForce * Time.deltaTime, ForceMode.VelocityChange);
            float floatForce = rb.velocity.y < 0 ? floatForceMax : floatForceMin;
            rb.AddForce(transform.up * floatForce * Time.deltaTime, ForceMode.Impulse);
            GetComponent<p_status>().UserEnergy(floatForce / 2 * Time.deltaTime);
        }
    }
    public bool isPerformingAttack() {
        return isAttacking;
    }

    public bool isPerformingSpecial() {
        return isSpecialCasting;
    }

    public void Block() {
        //StartCoroutine(PerformBlock());
        if (isBlocking == false) {
            isBlocking = true;
            canMove = false;
            anim.SetBool("isBlocking", true);
        }
    }

    public void StopBlock() {
        if (isBlocking == true) {
            canMove = true;
            isBlocking = false;
            anim.SetBool("isBlocking", false);
        }
    }

    public bool isPerformingBlock() {
        return isBlocking;
    }

    public bool isGrounded() {
        return isPlayerGrounded;
    }

    public void Die() {
        anim.SetTrigger("die");
        GetComponent<p_status>().Dead();
        anim.SetBool("isAlive", GetComponent<p_status>().isPlayerAlive());
        velocity = Vector3.zero;
    }

    public void Respawn() {
        // anim.SetTrigger("spawn");
        GetComponent<p_status>().Respawn();
        anim.SetBool("isAlive", GetComponent<p_status>().isPlayerAlive());
    }

    public void TakeDamage() {
        StartCoroutine(PerformTakeDamage());
    }

    public void UpdateInCombat(bool _isInCombat) {
        isInCombat = _isInCombat;
    }

    public bool IsInCombat() {
        return isInCombat;
    }
    #endregion

    #region IEnumerators
    IEnumerator PerformTakeDamage()
    {
        anim.SetTrigger("hit");
        canMove = false;
        isInCombat = true;
        GetComponent<p_status>().SetLastCombatTime(Time.time);
        yield return new WaitForSeconds(KnockbackInterval);
        canMove = true;
    }

    IEnumerator PerformSpecial() {
        isSpecialCasting = true;
        canMove = false; 
        isInCombat = true;
        GetComponent<p_status>().SetLastCombatTime(Time.time);
        anim.SetTrigger("special");
        GetComponent<p_status>().UserEnergy(SpecialModel.gameObject.GetComponent<Getsuga>().Cost);
        GameObject _Special = Instantiate(SpecialModel, SpawnPoint.position, SpawnPoint.rotation);
        Debug.Log("My Nr: " + GetComponent<PhotonView>().OwnerActorNr);
        _Special.GetComponent<Getsuga>().SetOwner(GetComponent<PhotonView>().OwnerActorNr);
        // _Special.GetComponent<PhotonView>().RPC("SetSpecialsOwnerRPC", RpcTarget.All, new object[] { GetComponent<PhotonView>().OwnerActorNr });
        // GameObject _SpecialPhoton = PhotonNetwork.Instantiate(this.SpecialModelPhoton.name, SpawnPoint.position, SpawnPoint.rotation);
        // _SpecialPhoton.GetComponent<Getsuga>().SetOwner(this.gameObject);
        yield return new WaitForSeconds(SpecialAnimation.length);
        isSpecialCasting = false;
        canMove = true;
    }

    IEnumerator PerformAttack() {
        isAttacking = true;
        canMove = false;
        isInCombat = true;
        GetComponent<p_status>().SetLastCombatTime(Time.time);
        anim.SetBool("isAttacking", isAttacking);
        anim.SetInteger("comboCounter", comboCounter);
        anim.SetTrigger("attack");
        yield return new WaitForSeconds(attackInterval);
        comboCounter = comboCounter == 1 ? 0 : 1;
        canMove = true;
        isAttacking = false;
        anim.SetBool("isAttacking", isAttacking);
    }

    IEnumerator PerformBlock() {
        isBlocking = true;
        canMove = false;
        anim.SetBool("isBlocking", true);
        yield return new WaitForSeconds(blockInterval);
        canMove = true;
        isBlocking = false;
        anim.SetBool("isBlocking", false);
    }

    IEnumerator PerformShunpo(Vector3 direction) {
        // canMove = false;
        shunpoMultiplier = 10f;
        model.SetActive(false);
        modelStatus = false;
        ShunpoBorder.fillAmount = 0;
        if (shunpoCharge == MaxShunpoCharge)
            shunpoCharger = ShunpoInterval + ShunpoSpeed;
        --shunpoCharge;
        // if(GetComponent<p_controller>().isMine == true)
        ShunpoCounter.text = shunpoCharge.ToString();
        yield return new WaitForSeconds(ShunpoSpeed);
        shunpoMultiplier = 1f;
        // rb.position = rb.position + direction * ShunpoRange;
        // canMove = true;
        model.SetActive(true);
        modelStatus = true;
    }
    #endregion

    #region RPC methods
    [PunRPC]
    public void SpecialRPC(Vector3 spawnPoint, Quaternion spawnRotation, int ownerNr) {
        // Debug.Log("Instantiate Getsuga of other player at: " + spawnPoint);
        // Debug.Log("Owner: " + ownerNr);
        GameObject _Special = Instantiate(SpecialModel, spawnPoint, spawnRotation);
        _Special.GetComponent<Getsuga>().SetOwner(ownerNr);
    }

    [PunRPC]
    public void RespawnRPC() {
        Respawn();
    }
    #endregion
    #region Photon Serialize Values

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            // We own this player: send the others our data
            // stream.SendNext(IsFiring);
            stream.SendNext(modelStatus);
            stream.SendNext(isBlocking);
            stream.SendNext(canMove);
            stream.SendNext(this.rb.position);
            stream.SendNext(this.rb.rotation);
            stream.SendNext(this.rb.velocity);
        }
        else
        {
            // Network player, receive data
            // this.IsFiring = (bool)stream.ReceiveNext();
            this.modelStatus = (bool)stream.ReceiveNext();
            this.isBlocking = (bool)stream.ReceiveNext();
            this.canMove = (bool)stream.ReceiveNext();
            networkPosition = (Vector3)stream.ReceiveNext();
            networkRotation = (Quaternion)stream.ReceiveNext();
            rb.velocity = (Vector3)stream.ReceiveNext();

            float lag = Mathf.Abs((float)(PhotonNetwork.Time - info.SentServerTime));
            networkPosition += (this.rb.velocity * lag);
        }
    }

    private void FixedUpdatePlayersPosition() {
        if (!GetComponent<PhotonView>().IsMine)
        {
            rb.position = Vector3.MoveTowards(rb.position, networkPosition, Time.fixedDeltaTime);
            rb.rotation = Quaternion.RotateTowards(rb.rotation, networkRotation, Time.fixedDeltaTime * 100.0f);
        }
    }

    private void UpdateShunpoStatus() {
        if (!GetComponent<PhotonView>().IsMine) {
            model.SetActive(modelStatus);
        }
    }
    #endregion
}
