﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class p_controller : MonoBehaviour
{
    // Public Variables
    public bool isMine;

    // Private Variables
    GameManager GM;
    Vector3 velocity = Vector3.zero;
    p_motor motor;
    p_status status;

    private void Awake()
    {
        GM = GameObject.Find("GameManager").GetComponent<GameManager>();
    }
    // Start is called before the first frame update
    void Start()
    {
        motor = GetComponent<p_motor>();
        status = GetComponent<p_status>();
    }
    // Update is called once per frame
    void Update()
    {
        // GetComponent<PhotonView>().IsMine == true use this in case Photon Network is enabled
        if (GM.isPhotonEnabled)
        {
            if (GetComponent<PhotonView>().IsMine == true && status.isPlayerAlive() == true)
            {

                ControlHandler();


                if (Input.GetKeyDown(KeyCode.L))
                {
                    // Debug.Log("Die");
                    motor.Die();
                }
            }

            if (GetComponent<PhotonView>().IsMine == true && status.isPlayerAlive() == false)
            {
                if (Input.GetKeyDown(KeyCode.K))
                {
                    // Debug.Log("Respawning on command");
                    motor.Respawn();
                }
            }
        }
        else {
            if (isMine == true && status.isPlayerAlive() == true)
            {

                ControlHandler();


                if (Input.GetKeyDown(KeyCode.L))
                {
                    // Debug.Log("Die");
                    motor.Die();
                }
            }

            if (isMine == true && status.isPlayerAlive() == false)
            {
                if (Input.GetKeyDown(KeyCode.K))
                {
                    // Debug.Log("Respawning on command");
                    motor.Respawn();
                }
            }
        }
        
    }

    #region Controller Logic

    void ControlHandler() {
        if (GM.RoundState() == true)
        {
            MovementControl();
            JumpControl();
            ShunpoControl();
            if (motor.isPerformingBlock() == false)
                AttackControl();
            SpecialControl();
        }

        BlockControl();
    }

    #endregion

    #region Action Methods

    void MovementControl(){
        float movX = Input.GetAxis("Horizontal");
        float movY = Input.GetAxis("Vertical");

        velocity = new Vector3(movX, 0, movY);
        motor.Move(velocity);
    }

    void ShunpoControl(){
        if(Input.GetKeyDown(KeyCode.LeftShift)){
            // Debug.Log("Perform Shunpo");
            motor.Shunpo();
        }
    }

    void AttackControl() {
        if (Input.GetButtonDown("Fire1") && motor.isPerformingAttack() == false) {
            // Debug.Log("Attack");
            motor.Attack();
        }
    }

    void BlockControl() {
        if (motor.isPerformingAttack() == false) {

            if (Input.GetButtonDown("Fire2") && motor.isPerformingBlock() == false)
            {
                // Start Blocking
                // Debug.Log("Block");
                motor.Block();
            }
            else if (Input.GetButton("Fire2") && motor.isPerformingBlock() == true) {
                // Continue Blocking
                motor.Block();
            }
            else if (Input.GetButtonUp("Fire2") && motor.isPerformingBlock() == true)
            {
                // Stop Blocking
                motor.StopBlock();
            }
        }
    }

    void JumpControl() {
        if (Input.GetButtonDown("Jump") && motor.isGrounded() == true)
        {
            motor.Jump();
        }
        else if (Input.GetButton("Jump") && motor.isGrounded() == false && status.GetCurrentEnergy() > 10) {
            motor.UpdateInCombat(true);
            motor.FloatInAir();
        }
    }

    void SpecialControl() {
        if (Input.GetKeyDown(KeyCode.E) && motor.isPerformingSpecial() == false)
        {
            motor.Special();
        }
    }

    #endregion
}
