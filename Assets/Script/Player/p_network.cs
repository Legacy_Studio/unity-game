﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

using Photon.Pun;
using Photon.Realtime;

public class p_network : MonoBehaviour
{
    public static p_network Instance;
    public TMP_InputField _inputField;
    public string PlayerName { get; private set; }

    #region Private Constants

    const string playerNamePrefKey = "PlayerName";

    #endregion
    // Start is called before the first frame update
    void Start()
    {
        string defaultName = string.Empty;
        if (_inputField != null)
        {
            if (PlayerPrefs.HasKey(playerNamePrefKey))
            {
                defaultName = PlayerPrefs.GetString(playerNamePrefKey);
                _inputField.text = defaultName;
            }
        }


        PhotonNetwork.NickName = defaultName;

        Instance = this;
    }

    #region Public Methods


    /// <summary>
    /// Sets the name of the player, and save it in the PlayerPrefs for future sessions.
    /// </summary>
    /// <param name="value">The name of the Player</param>
    public void SetPlayerName(string value)
    {   
        // #Important
        if (string.IsNullOrEmpty(_inputField.text))
        {
            Debug.LogError("Player Name is null or empty");
            return;
        }
        PhotonNetwork.NickName = _inputField.text;


        PlayerPrefs.SetString(playerNamePrefKey, _inputField.text);
        JoinToRoom();
    }

    public void JoinToRoom()
    {
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.IsOpen = true;
        roomOptions.IsVisible = true;
        roomOptions.MaxPlayers = 2;
        PhotonNetwork.JoinOrCreateRoom("Arena", roomOptions, TypedLobby.Default);
    }


    #endregion
}
